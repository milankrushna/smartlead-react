import React, { useState } from "react";
import * as service from "../services/httpCall";
// import { useNavigate } from "react-router-dom";

export default function CreateAccount() {
  // Define your request data here
  const [data, setData] = useState({
    formName: "",
    userName: "",
    fromEmail: "",
    smtpHost: "smtp.zoho.com",
    messagePerDay: "200",
    isDiffReplay: false,
    isDiffEmailRcvEmail: false,
    password: "",
    smtpPort: "465",
    smtpCom: "",
    imapHost: "imap.zoho.com",
    imapPort: "993",
    imapComType: "",
    minTimeGap: "",
  });
  // const navigate = useNavigate();

  const handleOnSubmit = (e) => {
    e.preventDefault();
    console.log(data);
    service
      .createAccount(data)
      .then((res) => {
        console.log(res.data);
        if (res.data.status === 1) {
          alert(res.data.message)
        } else {
          alert(res.data.message)
        }
      })
      .catch((err) => {
        // console.log(`Error from login api ->`, err);
        console.log(err.response.data.message);
        console.log(err.response.request.status);
        return alert(err.response.data.message);
      });

    // data = {}
  };

  const handleOnChange = (e) => {
    // console.log("n vdmnre", e.target.name, e.target.value);
    setData({
      ...data,
      [e.target.name]: e.target.value,
    });
  };

  return (
    <div className="container-fluid">
      <div className="justify-content-center mt-5 mb-5">
        <form onSubmit={handleOnSubmit}>

          <div class="col-md-12">

            <div class="row justify-content-center">
              <div class="col-md-5">
                <div class="mb-3">
                  <label for="inp1" className="form-label">
                    Form Name
                  </label>
                  <input
                    type="text"
                    class="form-control"
                    id="inp1"
                    name="formName"
                    value={data.formName}
                    onChange={handleOnChange}
                  />
                </div>
                <div class="mb-3">
                  <label for="exampleInputPassword1" class="form-label">
                    Username
                  </label>
                  <input
                    type="text"
                    class="form-control"
                    id="exampleInputPassword1"
                    name="userName"
                    value={data.userName}
                    onChange={handleOnChange}
                  />
                </div>
                <div class="mb-3">
                  <label for="inp2" class="form-label">
                    SMTP Host
                  </label>
                  <input
                    type="text"
                    class="form-control"
                    name="smtpHost"
                    id="inp2"
                    value={data.smtpHost}
                    onChange={handleOnChange}
                  />
                </div>
                <div class="mb-3">
                  <label for="inp3" class="form-label">
                    Message Per Day <i class="bi bi-info-circle"></i>
                  </label>
                  <input
                    type="text"
                    class="form-control"
                    name="messagePerDay"
                    id="inp3"
                    value={data.messagePerDay}
                    onChange={handleOnChange}
                  />
                </div>
                <div class="mb-3 form-check">
                  <input
                    type="checkbox"
                    class="form-check-input"
                    id="exampleCheck1"
                    name="isDiffReplay"
                    value={data.isDiffReplay}
                    onChange={handleOnChange}
                  />
                  <label class="form-check-label" for="exampleCheck1">
                    Set a different reply to address
                  </label>
                </div>
              </div>
              <div class="col-md-5">

                <div class="mb-3">
                  <label for="inp4" className="form-label">
                    Form Email
                  </label>
                  <input
                    type="email"
                    class="form-control"
                    id="inp4"
                    name="fromEmail"
                    value={data.fromEmail}
                    onChange={handleOnChange}
                  />
                </div>
                <div class="mb-3">
                  <label for="inp5" class="form-label">
                    Password
                  </label>
                  <input
                    type="password"
                    class="form-control"
                    id="inp5"
                    name="password"
                    value={data.password}
                    onChange={handleOnChange}
                  />
                </div>

                <div className="row">
                  <div className="col-3">
                    <div class="mb-3">
                      <label for="inp6" class="form-label">
                        SMTP Port
                      </label>
                      <input
                        type="text"
                        class="form-control"
                        id="inp6"
                        value={data.smtpPort}
                        name="smtpPort"
                        onChange={handleOnChange}
                      />
                    </div>
                  </div>
                  <div className="col-3 pt-4">
                    <div class="form-check">
                      <input
                        class="form-check-input"
                        type="radio"
                        checked="checked"
                        value={"SSL"}
                        name="smtpCom"
                        id="inp7"
                        onChange={handleOnChange}
                      />
                      <label class="form-check-label" for="inp7">
                        SSL
                      </label>
                    </div>
                  </div>

                  <div className="col-3 pt-4">
                    <div class="form-check">
                      <input
                        class="form-check-input"
                        type="radio"
                        name="smtpCom"
                        value={"TLS"}
                        id="inp8"
                        onChange={handleOnChange}
                      />
                      <label class="form-check-label" for="inp8">
                        TLS
                      </label>
                    </div>
                  </div>
                  <div className="col-3 pt-4">
                    <div class="form-check">
                      <input
                        class="form-check-input"
                        type="radio"
                        name="smtpCom"
                        id="inp9"
                        value={"None"}
                        onChange={handleOnChange}
                      />
                      <label class="form-check-label" for="inp9">
                        None
                      </label>
                    </div>
                  </div>
                </div>

                <div class="mb-3">
                  <label for="inp10" class="form-label">
                    Minimum Time Gap <i class="bi bi-info-circle"></i>
                  </label>
                  <input
                    type="text"
                    class="form-control"
                    id="inp10"
                    name="minTimeGap"
                    value={data.minTimeGap}
                    onChange={handleOnChange}
                  />
                </div>


                <div />

              </div>
            </div>
          </div>


          <div class="col-md-12">
            <div class="row justify-content-center  ">
              <div class="col-md-5">
                <h5 className="mt-5"> IMAP Settings (receives emails) </h5>
                <div class="mb-3 form-check">
                  <input
                    type="checkbox"
                    class="form-check-input"
                    id="inp11"
                    name="isDiffEmailRcvEmail"
                    value={data.isDiffEmailRcvEmail}
                    onChange={handleOnChange}
                  />
                  <label class="form-check-label" for="inp11">
                    Use different email accounts for receiving emails
                  </label>
                </div>
                <div class="mb-3">
                  <label for="inp12" class="form-label">
                    IMAP Host
                  </label>
                  <input
                    type="text"
                    class="form-control"
                    id="inp12"
                    value={data.imapHost}
                    name="imapHost"
                    onChange={handleOnChange}
                  />
                </div>
                <div />
                <div />
              </div>
              <div class="col-md-5">
                <div class="col-3"></div>
                <div className="row pt-5">
                  <div className="col-3">
                    <div class="mb-3">
                      <label for="inp13" class="form-label">
                        IMAP Port
                      </label>
                      <input
                        type="text"
                        class="form-control"
                        id="inp13"
                        name="imapPort"
                        value={data.imapPort}
                        onChange={handleOnChange}
                      />
                    </div>
                  </div>
                  <div className="col-3 pt-4">
                    <div class="form-check">
                      <input
                        class="form-check-input"
                        type="radio"
                        checked="checked"
                        name="imapComType"
                        id="inp15"
                        value={"SSL"}
                        onChange={handleOnChange}
                      />
                      <label class="form-check-label" for="inp15">
                        SSL
                      </label>
                    </div>
                  </div>

                  <div className="col-3 pt-4">
                    <div class="form-check">
                      <input
                        class="form-check-input"
                        type="radio"
                        name="imapComType"
                        id="inp14"
                        value={"TLS"}
                        onChange={handleOnChange}
                      />
                      <label class="form-check-label" for="inp14">
                        TLS
                      </label>
                    </div>
                  </div>
                  <div className="col-3 pt-4">
                    <div class="form-check">
                      <input
                        class="form-check-input"
                        type="radio"
                        name="imapComType"
                        id="inp16"
                        value={"None"}
                        onChange={handleOnChange}
                      />
                      <label class="form-check-label" for="inp16">
                        None
                      </label>
                    </div>
                  </div>
                </div>

              </div>
              <div class="col-md-4 text-center">
                <button
                  type="submit"
                  className="btn btn-lg btn-primary btn-block my-2">
                  Submit
                </button>
              </div>
            </div>

          </div>



        </form>

      </div>
    </div>

  );
}


// export default Login
