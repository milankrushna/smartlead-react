import React from "react";
import { Link } from "react-router-dom";

export default function Navbar() {
  return (
    // <!-- Navbar -->
    <nav className="navbar navbar-expand-lg navbar-dark" id="top_nav">
      <div className="container-fluid">
        <Link to="/" className="navbar-brand">
          <img
            src={require("../images/logo192.png")}
            alt="logo"
            className="img-fluid"
            id="nav_img"
          />
        </Link>

        {/* <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button> */}
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <Link to="/" className="nav-link active">
                Home
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/create_account" className="nav-link">
                Create Account
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/compose_email" className="nav-link">
                Compose Email
              </Link>
            </li>
            
          </ul>
          <form className="d-flex" role="search" id="searchform">
            <input
              className="form-control me-2"
              type="search"
              placeholder="Search..."
              aria-label="Search"
              id="searchbox"
            />
            <button
              className="btn btn-outline-success"
              id="searchbtn"
              type="submit"
            >
              <i className="bi bi-search"></i>
            </button>
          </form>
        </div>
      </div>
    </nav>
  );
}
