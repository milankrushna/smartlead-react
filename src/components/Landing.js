import React from "react";
import { useNavigate } from "react-router-dom";
export default function Landing() {

  let navigate = useNavigate(); 
  const routeChange = (path) =>{ 
    console.log(path);
    navigate(path);
  }
  return (
    // <!-- Container-fluid -->
    <div class="container-fluid">
      {/* <!-- Banner --> */}
      <div class="row justify-content-center" id="banner">
        <div class="col-md-6 col-11" id="bannertext">
          <h1>Lorem ipsum dolor, sit amet consectetur.</h1>
          <p>
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Tempora
            sit, quo quae placeat eum facere vel molestias asperiores officiis
            id nihil! Temporibus, eos?
          </p>
          <button type="button" onClick={()=> routeChange("/create_account")} class="btn" id="banner-btn-1">
            Create Account
          </button>
          <button type="button"  onClick={()=> routeChange("/compose_email")} class="btn" id="banner-btn-2">
           Compose Email
          </button>
        </div>
        <div class="col-md-4 d-none d-md-block" id="bannerimg">
          <img
            src={require("../images/logo512.png")}
            alt=""
            className="img-fluid"
          />
        </div>
      </div>
    </div>
  );
}
