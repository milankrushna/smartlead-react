import React, { useState } from "react";
import * as service from "../services/httpCall";

export default function ComposeEmail() {
  // Define your request data here
  const [data, setData] = useState({
    toEmail: "",
    subject: "",
    messageBody: ""
  });
  const handleOnSubmit = (e) => {
    e.preventDefault();
    console.log(data);
    service
      .composeEmail(data)
      .then((res) => {
        console.log(res.data);
        if (res.data.status === 1) {
          alert(res.data.message)
        } else {
          alert(res.data.message)
        }
      })
      .catch((err) => {
        // console.log(`Error from login api ->`, err);
        console.log(err.response.data.message);
        console.log(err.response.request.status);
        return alert(err.response.data.message);
      });
  };

  const handleOnChange = (e) => {
    // console.log("n vdmnre", e.target.name, e.target.value);
    setData({
      ...data,
      [e.target.name]: e.target.value,
    });
  };

  return (
    <div className="container-fluid">
      <div className="row justify-content-center mt-5 mb-5">
        <div className="col-md-5">
          <form onSubmit={handleOnSubmit}>
            <div class="mb-3">
              <label for="inp0" className="form-label">
                Email Address
              </label>
              <input
                type="email"
                class="form-control"
                id="inp0"
                name="toEmail"
                value={data.toEmail}
                onChange={handleOnChange}
                required
              />
            </div>
            <div class="mb-3">
              <label for="inp1" class="form-label">
                Subject
              </label>
              <input
                type="text"
                class="form-control"
                id="inp1"
                name="subject"
                value={data.subject}
                onChange={handleOnChange}
                required
              />
            </div>
            <div class="mb-3">
              <label for="inp2" class="form-label">
                Message Body
              </label>
              <textarea

                class="form-control"
                id="inp2"
                name="messageBody"
                value={data.messageBody}
                onChange={handleOnChange}
                required
              ></textarea>
            </div>

            <button
              type="submit"
              className="btn btn-lg btn-primary btn-block my-2"
            >
              Submit
            </button>
          </form>
        </div>
      </div>
    </div>

  );
  //   }
}

// export default Login
