
import axios from "axios";

const baseUrl = "http://localhost:8080";


  export const createAccount = (request_data)=> {
    console.log(`=========>>>`, request_data);

    let apiRequest = {
        from_name: request_data.formName,
        from_email : request_data.fromEmail,
        user_name: request_data.userName,
        password : request_data.password,
        smtp_host : request_data.smtpHost,
        smtp_port: request_data.smtpPort,
        smtp_com_type: request_data.smtpCom,
        message_per_day: request_data.messagePerDay,
        min_time_gap: request_data.minTimeGap,
        is_diff_replay: request_data.isDiffReplay,
        is_diff_email_to_rcv: request_data.isDiffEmailRcvEmail,
        imap_host: request_data.imapHost,
        imap_port: request_data.imapPort,
        imap_com_type: request_data.imapComType
    }

    return axios.post(baseUrl + "/api/create_account", apiRequest, {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    });
  }


  export const composeEmail = (request_data) => {
    console.log(`=========>>>`, request_data);

    let apiRequest = {
        to_email: request_data.toEmail,
        subject : request_data.subject,
        email_body : request_data.messageBody,
    }

    return axios.post(baseUrl + "/api/send_email", apiRequest, {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    });
  }
