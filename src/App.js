import './App.css';
import Navbar from "./components/Navbar";
import Landing from './components/Landing';
import CreateAccount from './components/CreateAccount';
import ComposeEmail from './components/ComposeEmail';
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

function App() {
  return (
    <Router>
    <div className="App">
      <Navbar />
      <Routes>
      <Route path="/" element={<Landing />} />
      <Route path="/create_account" element={<CreateAccount />} />
      <Route path="/compose_email" element={<ComposeEmail />} />
      </Routes>
    </div>
    </Router>
  );
}

export default App;
